const fs = require('fs');
const { TYPES, DEFAULT_FOLDERS, DEFAULT_BASE_PATH } = require('./config');
const pckg = require('../package.json');

const filterFolders = (options) => () => options.includes('--extended');

const getType = (type) => {
  const [typeCommand] = TYPES.find((typeVariants) => typeVariants.includes(type));
  return typeCommand;
};

const toCamelCase = (string) => string.split('-').map((item) =>
  `${item.charAt(0).toUpperCase()}${item.slice(1)}`).join('');

const generateIndexContent = (component, isSubcomponent = false) => {
  const componentPath = isSubcomponent ? `./${component}` : `./components/${component}`;
  return `export { default } from '${componentPath}';\n`
};

const generateComponentContent = (component) => {
  const componentDefinition = `const ${toCamelCase(component)} = () => {\n\treturn (\n\t\t<div>${component} component</div>\n\t);\n};\n\n`;
  const componentDefaultExport = `export default ${toCamelCase(component)};\n`;
  return `${componentDefinition}${componentDefaultExport}`;
}

/**
 * [basePath] = ./src/components
 *
 * [basePath]/[component-name]
 * [basePath]/[component-name]/index.tsx
 * [basePath]/[component-name]/components
 * [basePath]/[component-name]/components/[component-name]/index.tsx
 * [basePath]/[component-name]/components/[component-name]/[component-name].tsx
 * [basePath]/[component-name]/components/[component-name]/[component-name].module.scss
 */
const createComponent = ({ basePath = DEFAULT_BASE_PATH, component, isAlone }) => {
  const componentBasePath = `${basePath}/${component}`;
  const componentPath = isAlone ? componentBasePath : `${componentBasePath}/components/${component}`;

  if (fs.existsSync(componentPath)) {
    return;
  }

  try {
    fs.mkdirSync(componentPath, { recursive: true });

    if (!isAlone) {
      fs.writeFileSync(`${componentBasePath}/index.tsx`, generateIndexContent(component));
    }

    fs.writeFileSync(`${componentPath}/index.tsx`, generateIndexContent(component, true));
    fs.writeFileSync(`${componentPath}/${component}.tsx`, generateComponentContent(component));
    fs.writeFileSync(`${componentPath}/${component}.module.scss`, '');
  } catch (error) {
    console.log(error.message);
  }
};

const createAdditionalFolders = ({ componentBasePath, type, options, isAlone }) => {
  if (isAlone) {
    return;
  }

  DEFAULT_FOLDERS.filter(filterFolders(options)).forEach((folder) => {
    const additionalFolderPath = `${componentBasePath}/${folder}`;

    if (fs.existsSync(additionalFolderPath)) {
      return;
    }

    try {
      fs.mkdirSync(additionalFolderPath, { recursive: true });
    } catch (error) {
      console.log(error.message);
    }
  });
};

const confirmCreation = ({ componentBasePath, type }) => {
  console.log(`New ${getType(type)} added to:`);
  console.log(componentBasePath);
};

const generateHelp = () => {
  console.log(`
    ------------------
    | Version: ${pckg.version} |
    ------------------

    Commands:
      portal generate component [component-name] Generate a shared component into src/components folder
      portal generate feature [component-name]   Generate a feature specific component into src/features folder

    Aliases:
      generate  g
      component c
      feature   f

    Options:
      --extended  generate all folders into feature component (api, assets, constants, components, hooks, roots, types, utils, services)
  `);
};

module.exports = {
  toCamelCase,
  generateIndexContent,
  generateComponentContent,
  createComponent,
  createAdditionalFolders,
  confirmCreation,
  generateHelp,
};
