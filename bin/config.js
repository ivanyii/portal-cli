module.exports = {
  DEFAULT_BASE_PATH: './src/components',
  DEFAULT_COMPONENT_NAME: 'NewComponent',
  DEFAULT_FOLDERS: [
    'api',
    'assets',
    'constants',
    'components',
    'hooks',
    'roots',
    'types',
    'utils',
    'services',
  ],
  TYPES: [
    ['component', 'c'],
    ['feature', 'f'],
  ]
};
