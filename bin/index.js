#!/usr/bin/env node
const arguments = process.argv.slice(2);
const options = process.argv.slice(5)
const [action, type, name] = arguments;
const { DEFAULT_BASE_PATH, DEFAULT_COMPONENT_NAME } = require('./config');
const { createComponent, toCamelCase, generateHelp, confirmCreation, createAdditionalFolders } = require('./utils');

const generateComponent = ({ basePath = DEFAULT_BASE_PATH, component = DEFAULT_COMPONENT_NAME, isAlone }) => {
  component = toCamelCase(component);

  const componentBasePath = `${basePath}/${component}`;

  createComponent({ basePath, component, isAlone });
  createAdditionalFolders({ componentBasePath, type, options, isAlone });
  confirmCreation({ componentBasePath, type });
};

if (!['generate', 'g'].includes(action)) {
  generateHelp();
  return;
}

switch (type) {
  case 'feature':
  case 'f':
    /**
    * Generate feature component into ./src/feature folder
    */
    generateComponent({
      basePath: './src/features',
      component: name,
    });
    break;
  case 'component':
  case 'c':
    /**
    * Generate global/shared component into ./src/components folder
    */
    generateComponent({
      basePath: './src/components',
      component: name,
      isAlone: true,
    });
    break;
  default:
    break;
}
